# Three Little Pigs VR: An Example of VR Storytelling for Children
While most studies performed on the application of games to childhood education focus on traditional games, not much study has been directed at virtual reality (VR) games. VR is not a new technology, but only recently become commercially available in a widespread, affordable way in 2016 with the release of the Oculus Rift. Thus, there has been little impetus to study the technology in an educational setting until now. 
This project aimed to develop a VR Educational game that may enable future research projects. [This paper](https://gitlab.com/mwahnish/ed-tech/blob/master/Assets/mwahnish3%20CS%206460%20Final%20Paper.pdf) provides an overview of the reasons for undertaking this project, the architectural and technical design considerations that factored into development, game design, and potential opportunities for enhancement and future research.

## Prerequisites
* This software was build using [Unity3D 2018.2](https://unity3d.com/), although it may work with later versions as well.
* Oculus Rift and touch controllers. Other VR systems may work but are untested
* Steam VR must be installed on your system

## Setup
1. Unzip or clone this repo
2. Open the folder as a new project. The folder you select should be the directory above "Assets"
3. Either press play or make a build through File->Build And Run