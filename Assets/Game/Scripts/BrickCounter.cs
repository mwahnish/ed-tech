﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BrickCounter : MonoBehaviour {
    public int brickCount
    {
        get { return bricksInCounter.Count; }
    }

    List<System.Guid> bricksInCounter = new List<System.Guid>();

    [SerializeField]
    BoxCollider outerBounds;

    [SerializeField]
    BoxCollider innerBounds;

    [SerializeField]
    ParticleSystem effect;

    [SerializeField]
    AudioSource success;

    private void Start()
    {
        Events.instance.AddListener<ItemAtRest>(OnItemStill);
        Events.instance.AddListener<ItemMoved>(OnItemMoved);
    }

    private void Update()
    {

    }

    private void OnItemStill(ItemAtRest itemAtRest)
    {
        if (IsInSensor(itemAtRest.position) && !bricksInCounter.Contains(itemAtRest.itemAtRest))
        {
            bricksInCounter.Add(itemAtRest.itemAtRest);
            effect.Play();
            success.time = 0;
            success.Play();
        }
    }

    private void OnItemMoved(ItemMoved itemMoved)
    {
        bricksInCounter.Remove(itemMoved.itemAtRest);
    }

    private bool IsInSensor(Vector3 position)
    {
        return outerBounds.bounds.Contains(position) && !innerBounds.bounds.Contains(position);
    }
}
