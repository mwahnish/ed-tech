﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Character : MonoBehaviour {
    private Vector3 targetPosition;
	// Use this for initialization
	void Start () {
        targetPosition = transform.position;
	}

    public void Update()
    {
        transform.position = Vector3.MoveTowards(transform.position, targetPosition, 0.1f * Time.deltaTime);
    }

    public void MoveToMovePoint(string point)
    {
        CharacterMovePoint[] points = FindObjectsOfType<CharacterMovePoint>();
        CharacterMovePoint selection = null;
        foreach (CharacterMovePoint movePoint in points)
        {
            if (movePoint.id == point)
            {
                selection = movePoint;
                break;
            }
        }
        targetPosition = selection.transform.position;
    }
}
