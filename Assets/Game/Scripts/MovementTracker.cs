﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovementTracker : MonoBehaviour {

    bool wasStill;

    Vector3 lastPosition;

    float deadzone = 0.005f;

    Item cachedItem;

	// Use this for initialization
	void Start () {
        lastPosition = transform.position;
        cachedItem = GetComponent<Item>();
	}
	
	// Update is called once per frame
	void Update () {
        Vector3 currentPosition = transform.position;
        float delta = Vector3.Distance(lastPosition, currentPosition);
        if (delta > deadzone && wasStill)
        {
            Events.instance.Raise(new ItemMoved(cachedItem.guid));
            wasStill = false;
        }else if (delta <= deadzone && !wasStill && !cachedItem.isPickedUp)
        {
            Events.instance.Raise(new ItemAtRest(cachedItem.guid, this.transform.position));
            wasStill = true;
        }
        lastPosition = currentPosition;
	}
}
