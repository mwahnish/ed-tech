﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using ABXY.Stately;

public class Item : MonoBehaviour {
    public System.Guid guid
    {
        get; private set;
    }

    public enum ItemState { DROPPED, PICKED_UP, REST}

    [SerializeField]
    private StateMachine itemStateMachine = new StateMachine(typeof(ItemState));

    private Rigidbody cachedRigidBody;

    private Collider itemCollider;

    private Transform pickedUpTarget;

    private Transform spawnPoint;

    private float respawnAt = -10f;

    private Transform originalParent;

    public bool isPickedUp
    {
        get { return itemStateMachine.CurrentStateIs(ItemState.PICKED_UP); }
    }

	// Use this for initialization
	void Start () {

        originalParent = FindObjectOfType<ItemSpawn>().transform;
        guid = System.Guid.NewGuid();
        cachedRigidBody = GetComponent<Rigidbody>();

        itemCollider = GetComponent<Collider>();

        SetupSpawn();

        Events.instance.AddListener<ItemPickup>(OnPickUpEvent);
        Events.instance.AddListener<ItemPutDown>(OnPutDownEvent);
        Events.instance.AddListener<OnWolfBlow>(OnWolfBlow);

        itemStateMachine.SetDefaultState(ItemState.DROPPED);
        itemStateMachine.AddHandler(StatelyEnums.HandlerType.OnActivate, OnPickedUpStart(), ItemState.PICKED_UP);
        itemStateMachine.AddHandler(StatelyEnums.HandlerType.WhileActivated, WhilePickedUp, ItemState.PICKED_UP);
        itemStateMachine.AddHandler(StatelyEnums.HandlerType.OnActivate, OnDroppedStart, ItemState.DROPPED);
        itemStateMachine.Start(this);
	}

    private void Update()
    {
        itemStateMachine.Update();
        SetupSpawn();
        CheckForRespawn();
    }

    #region event handlers
    private void OnPickUpEvent(ItemPickup pickup)
    {
        if (pickup.pickedUpItem == guid)
        {
            pickedUpTarget = pickup.target;
            itemStateMachine.ActivateState(ItemState.PICKED_UP);
        }
    }

    private void OnPutDownEvent(ItemPutDown putdown)
    {
        if (putdown.putDownItem == guid)
        {
            itemStateMachine.ActivateState(ItemState.DROPPED);
        }
    }
    #endregion

    #region States
    private IEnumerable OnPickedUpStart()
    {
        itemCollider.enabled = false;
        cachedRigidBody.isKinematic = true;
        transform.parent = pickedUpTarget;

        Vector3 startPos = transform.localPosition;
        for (float index = 0; index < 1; index += 20f * Time.deltaTime)
        {
            transform.localPosition = Vector3.Lerp(startPos, Vector3.zero, index);
            yield return null;
        }
        transform.localPosition = Vector3.zero;

        
    }

    private void WhilePickedUp()
    {
    }

    private void OnDroppedStart()
    {
        transform.parent = originalParent;
        pickedUpTarget = null;
        itemCollider.enabled = true;
        cachedRigidBody.isKinematic = false;
    }
    #endregion

    private void SetupSpawn()
    {
        if (spawnPoint == null)
            spawnPoint = FindObjectOfType<ItemSpawn>().transform;
    }

    private void CheckForRespawn()
    {
        if (this.transform.position.y < respawnAt)
        {
            this.transform.position = spawnPoint.position;
            this.GetComponent<Rigidbody>().velocity = Vector3.zero;
        }
    }

    private void OnWolfBlow(OnWolfBlow onWolfBlow)
    {
        bool push = transform.position.x < onWolfBlow.source.x;
        //bool push = true;
        if (itemStateMachine.CurrentStateIs(ItemState.DROPPED) && push)
        {
            this.GetComponent<Rigidbody>().AddExplosionForce(onWolfBlow.force, onWolfBlow.source, 5f, .1f);
        }
        if (onWolfBlow.destroy)
            StartCoroutine(DestroyInXSeconds(2f));
    }

    private void OnDestroy()
    {
        Events.instance.RemoveListener<ItemPickup>(OnPickUpEvent);
        Events.instance.RemoveListener<ItemPutDown>(OnPutDownEvent);
        Events.instance.RemoveListener<OnWolfBlow>(OnWolfBlow);
    }

    private IEnumerator DestroyInXSeconds(float x)
    {
        yield return new WaitForSeconds(x);
        Destroy(gameObject);
    }
}
