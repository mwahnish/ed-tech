﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveTo : MonoBehaviour {
    [SerializeField]
    Transform target;

    Rigidbody cachedRigidBody;

    [SerializeField]
    float force = 1f;

	// Use this for initialization
	void Start () {
        cachedRigidBody = GetComponent<Rigidbody>();
	}
	
	// Update is called once per frame
	void Update () {
        if (target != null)
            MoveToTarget();
	}

    private void MoveToTarget()
    {
        Vector3 direction = target.position - this.transform.position;
        cachedRigidBody.AddForce(direction * force, ForceMode.Force);
    }
}
