﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemSpawn : MonoBehaviour {

    [SerializeField]
    GameObject prototype;

    [SerializeField]
    int number = 20;

	// Use this for initialization
	void Start () {
        
	}

    public void SpawnBricks()
    {
        StartCoroutine(Spawn());
    }

    private IEnumerator Spawn()
    {
        for (int index = 0; index < number; index++)
        {
            //GameObject go = GameObject.Instantiate(prototype, this.transform.position, Quaternion.identity);
            GameObject go = Instantiate(prototype, this.transform);
            go.transform.position = this.transform.position + new Vector3(Random.Range(-.1f, .1f), Random.Range(-.1f, .1f), Random.Range(-.1f, .1f));
            yield return new WaitForSeconds(0.1f);
        }
    }
	
    public void SetBrickPrototype(GameObject proto)
    {
        this.prototype = proto;
    }

	// Update is called once per frame
	void Update () {
		
	}
}
