﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterAnimator : MonoBehaviour {

    Vector3 lookTarget = Vector3.right;

    Vector3 lastPosition;

    [SerializeField]
    Animator animator;

	// Use this for initialization
	void Start () {
        lastPosition = this.transform.position;
    }
	
	// Update is called once per frame
	void Update () {
        UpdateDirection();
        this.transform.LookAt(lookTarget,Vector3.up);
        
	}

    public void LookAtPlayer()
    {
        SteamVR_Camera camera = FindObjectOfType<SteamVR_Camera>();
        LookAtTransform(camera.transform);
    }

    public void LookAtTransform(Transform target)
    {
        lookTarget = new Vector3(target.position.x, transform.position.y, target.position.z);
    }

    private void UpdateDirection()
    {
        Vector3 currentPosition = transform.position;
        float distance = Vector3.Distance(currentPosition, lastPosition);
        if (animator != null)
            animator.SetFloat("speed", distance * 1000f);
        if (distance > 0.001)
        {
            Vector3 direction = currentPosition - lastPosition;
            direction.Normalize();
            lookTarget = transform.position + direction;
        }
        lastPosition = this.transform.position;
    }
}
