﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HandControllerCollision : MonoBehaviour {

    [SerializeField]
    private string handID;

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    private void OnTriggerEnter(Collider other)
    {
        Events.instance.Raise(new HandColliderEnter(other, handID));
    }

    private void OnTriggerExit(Collider other)
    {
        Events.instance.Raise(new HandColliderExit(other, handID));
    }
}
