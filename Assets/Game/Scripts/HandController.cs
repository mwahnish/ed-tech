﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using ABXY.Stately;

public class HandController : MonoBehaviour {
    [SerializeField]
    private string handID;

    private List<Collider> touchedColliders = new List<Collider>();

    enum handStates { COLLIDING, NOT_COLLIDING, HOLDING}

    [SerializeField]
    private StateMachine handControllerStates = new StateMachine(typeof(handStates));

    private GameObject currentCollidingItem;

    public Transform vis;

	// Use this for initialization
	void Start () {
        Events.instance.AddListener<HandColliderEnter>(OnHandColliderEnter);
        Events.instance.AddListener<HandColliderExit>(OnHandColliderExit);

        handControllerStates.AddHandler(StatelyEnums.HandlerType.WhileActivated, WhileCollisionState, handStates.COLLIDING);
        handControllerStates.AddHandlers(OnHoldingStateStart, WhileHoldingState, OnHoldingStateEnd, handStates.HOLDING);
        handControllerStates.SetDefaultState(handStates.NOT_COLLIDING);
        handControllerStates.Start(this);
    }
	
	// Update is called once per frame
	void Update () {
        handControllerStates.Update();
        bool closed = GetTriggerInput();
        if (closed)
            vis.localScale = new Vector3(0.01f, 0.01f, 0.01f);
        else
            vis.localScale = new Vector3(0.015f, 0.015f, 0.015f);

    }

    #region states
    private void WhileCollisionState()
    {
        if (GetTriggerInput())
        {
            handControllerStates.ActivateState(handStates.HOLDING);
        }
    }

    private void OnHoldingStateStart()
    {
        Events.instance.Raise(new ItemPickup(currentCollidingItem.GetComponent<Item>().guid, this.transform));
    }

    private void WhileHoldingState()
    {
        if (!GetTriggerInput())
        {
            handControllerStates.ActivateState(handStates.NOT_COLLIDING);
        }
    }

    private void OnHoldingStateEnd()
    {
        Events.instance.Raise(new ItemPutDown(currentCollidingItem.GetComponent<Item>().guid));
        currentCollidingItem = null;
    }

    #endregion

    #region events
    private void OnHandColliderEnter(HandColliderEnter enter)
    {
        if (enter.sourceName == handID && enter.other.GetComponent<Item>() != null)
        {
            if (currentCollidingItem == null && handControllerStates.CurrentStateIs(handStates.NOT_COLLIDING)) {
                currentCollidingItem = enter.other.gameObject;
                handControllerStates.ActivateState(handStates.COLLIDING);
            }
        }
    }

    private void OnHandColliderExit(HandColliderExit exit)
    {
        if (exit.sourceName == handID && exit.other.gameObject == currentCollidingItem)
        {
            if (handControllerStates.CurrentStateIs(handStates.COLLIDING))
            {
                currentCollidingItem = null;
                handControllerStates.ActivateState(handStates.NOT_COLLIDING);
            }
        }
    }
    #endregion

    private bool GetTriggerInput()
    {
        if (handID == "left")
        {
            return Input.GetAxis("LeftTrigger") > 0.3f;
        }
        else
        {
            return Input.GetAxis("RightTrigger") > 0.3f;
        }
    }
}
