﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemMoved : GameEvent {

    public System.Guid itemAtRest
    {
        get;
        private set;
    }
    public ItemMoved(System.Guid itemGUID)
    {
        itemAtRest = itemGUID;
    }
}
