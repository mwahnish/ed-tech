﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OnWolfBlow : GameEvent {

    public Vector3 source
    {
        get; private set;
    }

    public float force
    {
        get; private set;
    }

    public bool destroy
    {
        get; private set;
    }

	public OnWolfBlow(Vector3 source, float force, bool destroy)
    {
        this.source = source;
        this.force = force;
        this.destroy = destroy;
    }
}
