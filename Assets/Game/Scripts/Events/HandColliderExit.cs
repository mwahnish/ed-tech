﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HandColliderExit : GameEvent {

    public Collider other
    {
        get;
        private set;
    }

    public string sourceName
    {
        get;
        private set;
    }

	public HandColliderExit(Collider other, string sourceName)
    {
        this.other = other;
        this.sourceName = sourceName;
    }
}
