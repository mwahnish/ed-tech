﻿using System.Collections;
using System.Collections.Generic;



public class Events
{

    private static Events _instance;
    public static Events instance
    {
        get
        {
            if (_instance == null)
                _instance = new Events();
            return _instance;
        }
    }

    private Dictionary<System.Type, System.Delegate> listenersByEventType = new Dictionary<System.Type, System.Delegate>();

    public delegate void EventDelegate<T>(T eventClass);

    public void AddListener<T>(EventDelegate<T> del) where T : GameEvent
    {
        System.Type eventType = typeof(T);
        if (!listenersByEventType.ContainsKey(eventType))
        {
            listenersByEventType.Add(eventType, del);
        }
        else
        {
            System.Delegate currentDel = listenersByEventType[eventType];
            currentDel = System.Delegate.Combine(currentDel, del);
            listenersByEventType[eventType] = currentDel;
        }
            
    }

    public void RemoveListener<T>(EventDelegate<T> del) where T : GameEvent
    {
        System.Type eventType = typeof(T);
        if (listenersByEventType.ContainsKey(eventType))
        {
            System.Delegate currentDel = listenersByEventType[eventType];
            currentDel = System.Delegate.Remove(currentDel, del);
            listenersByEventType[eventType] = currentDel;
        }
    }

    public void Raise(GameEvent e)
    {
        System.Type eventType = e.GetType();
        if (listenersByEventType.ContainsKey(eventType))
        {
            System.Delegate currentDel = listenersByEventType[eventType];
            currentDel.DynamicInvoke(e);
        }
    }
}

public class GameEvent
{
}