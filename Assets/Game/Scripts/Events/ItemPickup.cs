﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemPickup : GameEvent
{

    public System.Guid pickedUpItem
    {
        get;
        private set;
    }

    public Transform target;

	public ItemPickup(System.Guid itemGUID, Transform target)
    {
        pickedUpItem = itemGUID;
        this.target = target;
    }
}
