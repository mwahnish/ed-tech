﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemPutDown : GameEvent {

    public System.Guid putDownItem
    {
        get;
        private set;
    }

    public ItemPutDown(System.Guid itemGUID)
    {
        putDownItem = itemGUID;
    }
}
