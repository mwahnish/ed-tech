﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemAtRest : GameEvent {

    public System.Guid itemAtRest
    {
        get;
        private set;
    }

    public Vector3 position
    {
        get;
        private set;
    }

    public ItemAtRest(System.Guid itemGUID, Vector3 position)
    {
        itemAtRest = itemGUID;
        this.position = position;
    }
}
