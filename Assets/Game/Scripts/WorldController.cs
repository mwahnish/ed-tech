﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using ABXY.Stately;

public class WorldController : MonoBehaviour {

    public enum phases { OPENING_DIALOG, BUILD1, WOLF_ENCOUNTER1, SCENE_TRANSITION1, OPENING_DIALOG_SCENE2, BUILD2, WOLF_ENCOUNTER2,
        OPENING_DIALOG_SCENE3, BUILD3, WOLF_ENCOUNTER3
    }

    [SerializeField]
    DialogList openingDialog;

    [SerializeField]
    DialogList buildDialog;

    [SerializeField]
    DialogList wolfEncounter1Dialog;

    [SerializeField]
    DialogList sceneTransition1Dialog;

    [SerializeField]
    DialogList openingDialogScene2;

    [SerializeField]
    DialogList wolfEncounter2Dialog;

    [SerializeField]
    DialogList openingDialogScene3;

    [SerializeField]
    DialogList wolfEncounter3Dialog;

    [SerializeField]
    BrickCounter build1BrickCounter;

    [SerializeField]
    BrickCounter build2BrickCounter;

    [SerializeField]
    BrickCounter build3BrickCounter;


    [SerializeField]
    WolfBlow wolfBlow;

    [SerializeField]
    Animator sceneTransition;

    [SerializeField]
    private StateMachine phaseMachine = new StateMachine(typeof(phases));

	// Use this for initialization
	void Start () {
        phaseMachine.AddHandler(StatelyEnums.HandlerType.OnActivate, OnOpeningDialogStart, phases.OPENING_DIALOG);
        phaseMachine.AddHandler(StatelyEnums.HandlerType.OnActivate, OnBuild1PhaseStart, phases.BUILD1);
        phaseMachine.AddHandler(StatelyEnums.HandlerType.WhileActivated, WhileBuild1Phase, phases.BUILD1);
        phaseMachine.AddHandler(StatelyEnums.HandlerType.OnActivate, OnWolfPhase1Start, phases.WOLF_ENCOUNTER1);
        phaseMachine.AddHandler(StatelyEnums.HandlerType.OnActivate, OnSceneTransition1Start, phases.SCENE_TRANSITION1);
        phaseMachine.AddHandler(StatelyEnums.HandlerType.OnActivate, OnOpeningDialogScene2Start, phases.OPENING_DIALOG_SCENE2);
        phaseMachine.AddHandler(StatelyEnums.HandlerType.WhileActivated, WhileBuild2Phase, phases.BUILD2);
        phaseMachine.AddHandler(StatelyEnums.HandlerType.OnActivate, OnWolfEncounter2Start, phases.WOLF_ENCOUNTER2);
        phaseMachine.AddHandler(StatelyEnums.HandlerType.OnActivate, OnOpeningScene3, phases.OPENING_DIALOG_SCENE3);
        phaseMachine.AddHandler(StatelyEnums.HandlerType.WhileActivated, WhileBuild3, phases.BUILD3);
        phaseMachine.AddHandler(StatelyEnums.HandlerType.OnActivate, OnWolfEncounter3, phases.WOLF_ENCOUNTER3);
        phaseMachine.SetDefaultState(phases.OPENING_DIALOG);
        phaseMachine.Start(this);
    }

    private void Update()
    {
        phaseMachine.Update();
        if (Input.GetKeyDown(KeyCode.Escape))
            Application.Quit();
    }

    private void OnOpeningDialogStart()
    {
        openingDialog.Trigger("start");
    }

    public void FinishOpeningDialog()
    {
        phaseMachine.ActivateState(phases.BUILD1);
    }

    private void OnBuild1PhaseStart()
    {
        buildDialog.Trigger("start");
    }

    private void WhileBuild1Phase()
    {
        if (build1BrickCounter.brickCount >= 12)
        {
            buildDialog.Trigger("build1_finished");
        }
    }

    public void FinishBuild1()
    {
        phaseMachine.ActivateState(phases.WOLF_ENCOUNTER1);
    }

    private void OnWolfPhase1Start()
    {
        wolfEncounter1Dialog.Trigger("start");
    }

    public void FinishWolf1Phase()
    {
        wolfBlow.StartBlow(true);
        phaseMachine.ActivateState(phases.SCENE_TRANSITION1);
    }

    private void OnSceneTransition1Start()
    {
        sceneTransition1Dialog.Trigger("start");
    }

    public void TriggerSceneTransitionAnimation()
    {
        sceneTransition.SetTrigger("MoveToSecondStage");
        StartCoroutine(StartScene2());
    }

    private IEnumerator StartScene2()
    {
        yield return new WaitForSeconds(5f);
        phaseMachine.ActivateState(phases.OPENING_DIALOG_SCENE2);
    }

    private void OnOpeningDialogScene2Start()
    {
        openingDialogScene2.Trigger("start");
    }

    public void FinishOpeningDialog2()
    {
        phaseMachine.ActivateState(phases.BUILD2);
    }

    private void WhileBuild2Phase()
    {
        if (build2BrickCounter.brickCount >= 12)
        {
            phaseMachine.ActivateState(phases.WOLF_ENCOUNTER2);
        }
    }

    private void OnWolfEncounter2Start()
    {
        wolfEncounter2Dialog.Trigger("start");
    }

    public void TriggerScene2TransitionAnimation()
    {
        sceneTransition.SetTrigger("MoveToThirdStage");
        StartCoroutine(StartScene3());
    }

    private IEnumerator StartScene3()
    {
        yield return new WaitForSeconds(5f);
        phaseMachine.ActivateState(phases.OPENING_DIALOG_SCENE3);
    }

    private void OnOpeningScene3()
    {
        openingDialogScene3.Trigger("start");
    }

    public void OpeningScene3Complete()
    {
        phaseMachine.ActivateState(phases.BUILD3);
    }

    private void WhileBuild3()
    {
        if (build3BrickCounter.brickCount >= 12)
        {
            phaseMachine.ActivateState(phases.WOLF_ENCOUNTER3);
        }
    }

    private void OnWolfEncounter3()
    {
        wolfEncounter3Dialog.Trigger("start");
    }
}
