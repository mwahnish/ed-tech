﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WolfBlow : MonoBehaviour {

    public float force = 5f;

    [SerializeField]
    private ParticleSystem effect;

    [SerializeField]
    private AudioSource audio;

	public void StartBlow(bool destroy)
    {
        Events.instance.Raise(new OnWolfBlow(this.transform.position, force, destroy));
        BlowEffect();
    }

    public void BlowEffect()
    {
        if (effect != null)
            effect.Play();
        if (audio != null)
        {
            audio.time = 0;
            audio.Play();
        }
    }
}
