﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class FadeController : MonoBehaviour {
    Material[] materials;
    Transform target;
	// Use this for initialization
	void Start () {
        Renderer renderer = GetComponent<Renderer>();
        if (Application.isPlaying)
            materials = renderer.materials;
        else
            materials = renderer.sharedMaterials;
        target = FindObjectOfType<FadeTarget>().transform;
	}
	
	// Update is called once per frame
	void Update () {
        if (target == null)
            target = FindObjectOfType<FadeTarget>().transform;
        else
        {
            foreach (Material mat in materials)
            {
                mat.SetVector("_Center", target.position);
               
            }
        }
	}
}
