﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class EnableController: MonoBehaviour {
    Transform target;

    [SerializeField]
    GameObject targetMono;

    [SerializeField]
    float radius = .28f;
	// Use this for initialization
	void Start () {
        target = FindObjectOfType<FadeTarget>().transform;
	}
	
	// Update is called once per frame
	void Update () {
        if (target == null)
            target = FindObjectOfType<FadeTarget>().transform;
        else
        {
            bool enabled = Vector3.Distance(target.position, this.transform.position) < radius;
            targetMono.SetActive(enabled);
        }
	}
}
