﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DialogTrigger : Dialog {

    private bool triggered = false;

    [SerializeField]
    private string _ID = "";

    public string id
    {
        get { return _ID; }
    }
    
    public void Trigger()
    {
        triggered = true;
    }

    public override void Begin(MonoBehaviour go)
    {
        if (!started)
            go.StartCoroutine(Run());
    }

    private IEnumerator Run()
    {
        started = true;
        while (!triggered)
        {
            yield return null;
        }
        complete = true;
    }
}
