﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class DialogWait : Dialog {

    [SerializeField]
    float waitTime = 1f;

    public override void Begin(MonoBehaviour go)
    {
        if (!started)
            go.StartCoroutine(Run());
    }

    private IEnumerator Run()
    {
        started = true;
        yield return new WaitForSeconds(waitTime);
        complete = true;
    }
}
