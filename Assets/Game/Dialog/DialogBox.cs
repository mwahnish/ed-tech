﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DialogBox : MonoBehaviour {

    Transform camera;

    [SerializeField]
    Transform dialog;

    private Transform source;

    [SerializeField]
    private Text text;

    public bool visible { get; private set; }
    BoxCollider collider;

	// Use this for initialization
	void Start () {
        camera = FindObjectOfType<SteamVR_Camera>().transform;
        collider = GetComponent<BoxCollider>();
	}
	
	// Update is called once per frame
	void Update () {
        dialog.LookAt(camera, Vector3.up);
        this.transform.position = source.position;
        UpdateVisibility();
	}

    private void UpdateVisibility()
    {
        Plane[] planes = GeometryUtility.CalculateFrustumPlanes(camera.GetComponent<Camera>());
        visible = GeometryUtility.TestPlanesAABB(planes, collider.bounds);
    }

    public void AnimationDestroy()
    {
        Destroy(gameObject);
    }

    private void SetText(string text)
    {
        this.text.text = text;
    }

    public static DialogBox Spawn(DialogSource source, string text)
    {
        GameObject box = Resources.Load<GameObject>("Dialog Box");
        GameObject newBox = GameObject.Instantiate(box);
        DialogBox boxScript = newBox.GetComponent<DialogBox>();
        boxScript.Start();
        boxScript.source = source.transform;
        boxScript.SetText(text);
        return boxScript;
    }

    public void Despawn()
    {
        Animator animator = GetComponent<Animator>();
        animator.SetTrigger("Close");
    }
}
