﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class DialogList : MonoBehaviour {

    [SerializeField]
    List<Dialog> dialogBlocks = new List<Dialog>();

    public bool finished
    {
        get; private set;
    }

    private void Update()
    {
        if (dialogBlocks.Count > 0)
        {
            Dialog current = dialogBlocks[0];
            if (!current.started)
                current.Begin(this);
            if (current.complete)
                dialogBlocks.RemoveAt(0);
        }
        else
        {
            finished = true;
        }
    }

    public void Trigger (string id)
    {
        foreach(Dialog dialog in dialogBlocks)
        {
            if (dialog.GetType() == typeof(DialogTrigger))
            {
                DialogTrigger trigger = dialog as DialogTrigger;
                if (trigger.id == id)
                    trigger.Trigger();
            }
        }
    }
}
