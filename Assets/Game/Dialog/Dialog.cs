﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Dialog : ScriptableObject{

    public bool complete
    {
        get; protected set;
    }

    public bool started
    {
        get; protected set;
    }

    public virtual void Begin(MonoBehaviour go)
    {

    }

}
