﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class DialogBlock : Dialog {

    [SerializeField]
    public string dialog = "";

    [SerializeField]
    public DialogSource source;

    [SerializeField]
    public float time = 2f;

    private const float naturalPause = 1f;

    public override void Begin(MonoBehaviour go)
    {
        if (!started)
            go.StartCoroutine(Run());
    }

    private IEnumerator Run()
    {
        started = true;
        if (source != null)
        {
            DialogBox box = DialogBox.Spawn(source, dialog);
            while (!box.visible)
                yield return null;
            yield return new WaitForSeconds(time);
            box.Despawn();
            yield return new WaitForSeconds(naturalPause);
        }
        complete = true;
    }
}
