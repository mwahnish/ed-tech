﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using Malee.Editor;

[CustomEditor(typeof(DialogList))]
public class DialogEditor : Editor {

    ReorderableList dialogList;

    private void OnEnable()
    {
        SerializedProperty prop = serializedObject.FindProperty("dialogBlocks");
        dialogList = new ReorderableList(prop);
        dialogList.onAddDropdownCallback += AddDropDown;
        dialogList.drawElementCallback += DrawElement;
        dialogList.getElementHeightCallback += GetElementHeight;
    }

    public void DrawElement(Rect rect, SerializedProperty element, GUIContent label, bool selected, bool focused)
    {
        SerializedObject so = new SerializedObject(element.objectReferenceValue);
        so.Update();
        System.Type type = so.targetObject.GetType();
        Vector2 pos = new Vector2(rect.position.x, rect.position.y);

        if (type == typeof(DialogBlock))
        {
            pos.y += EditorGUIUtility.standardVerticalSpacing;
            DrawTitle(new Rect(pos.x, pos.y, rect.width, EditorGUIUtility.singleLineHeight), "Dialog");
            pos.y += EditorGUIUtility.singleLineHeight + EditorGUIUtility.standardVerticalSpacing;
            EditorGUI.PropertyField(new Rect(pos.x, pos.y, rect.width, EditorGUIUtility.singleLineHeight), so.FindProperty("dialog"));
            pos.y += EditorGUIUtility.singleLineHeight + EditorGUIUtility.standardVerticalSpacing;
            EditorGUI.PropertyField(new Rect(pos.x, pos.y, rect.width, EditorGUIUtility.singleLineHeight), so.FindProperty("time"));
            pos.y += EditorGUIUtility.singleLineHeight + EditorGUIUtility.standardVerticalSpacing;
            EditorGUI.PropertyField(new Rect(pos.x, pos.y, rect.width, EditorGUIUtility.singleLineHeight), so.FindProperty("source"));
            pos.y += EditorGUIUtility.standardVerticalSpacing;
        }
        else if (type == typeof(DialogWait))
        {
            pos.y += EditorGUIUtility.standardVerticalSpacing;
            DrawTitle(new Rect(pos.x, pos.y, rect.width, EditorGUIUtility.singleLineHeight), "Wait");
            pos.y += EditorGUIUtility.singleLineHeight + EditorGUIUtility.standardVerticalSpacing;
            EditorGUI.PropertyField(new Rect(pos.x, pos.y, rect.width, EditorGUIUtility.singleLineHeight), so.FindProperty("waitTime"));
            pos.y += EditorGUIUtility.standardVerticalSpacing;
        }
        else if (type == typeof(DialogTrigger))
        {
            pos.y += EditorGUIUtility.standardVerticalSpacing;
            DrawTitle(new Rect(pos.x, pos.y, rect.width, EditorGUIUtility.singleLineHeight), "Trigger");
            pos.y += EditorGUIUtility.singleLineHeight + EditorGUIUtility.standardVerticalSpacing;
            EditorGUI.PropertyField(new Rect(pos.x, pos.y, rect.width, EditorGUIUtility.singleLineHeight), so.FindProperty("_ID"));
            pos.y += EditorGUIUtility.standardVerticalSpacing;
        }
        else if (type == typeof(DialogDelegate))
        {
            pos.y += EditorGUIUtility.standardVerticalSpacing;
            DrawTitle(new Rect(pos.x, pos.y, rect.width, EditorGUIUtility.singleLineHeight), "Delegate");
            pos.y += EditorGUIUtility.singleLineHeight + EditorGUIUtility.standardVerticalSpacing;
            EditorGUI.PropertyField(new Rect(pos.x, pos.y, rect.width, EditorGUIUtility.singleLineHeight), so.FindProperty("OnExecute"));
            pos.y += EditorGUIUtility.standardVerticalSpacing;
        }
        so.ApplyModifiedProperties();
    }

    public float GetElementHeight(SerializedProperty element)
    {
        SerializedObject so = new SerializedObject(element.objectReferenceValue);
        so.Update();
        System.Type type = so.targetObject.GetType();

        if (type == typeof(DialogBlock))
        {
            return (4 * EditorGUIUtility.singleLineHeight) + (5 * EditorGUIUtility.standardVerticalSpacing);

        }
        else if (type == typeof(DialogWait))
        {
            return (2 * EditorGUIUtility.singleLineHeight) + (3 * EditorGUIUtility.standardVerticalSpacing);
        }
        else if (type == typeof(DialogTrigger))
        {
            return (2 * EditorGUIUtility.singleLineHeight) + (3 * EditorGUIUtility.standardVerticalSpacing);
        }
        else if (type == typeof(DialogDelegate))
        {
            float propHeight = EditorGUI.GetPropertyHeight(so.FindProperty("OnExecute"));
            return (1 * EditorGUIUtility.singleLineHeight) + propHeight + (3 * EditorGUIUtility.standardVerticalSpacing);
        }
        return 0f;
    }

    private void DrawTitle(Rect pos, string title)
    {
        Vector2 dimensions = EditorStyles.boldLabel.CalcSize(new GUIContent(title));
        //EditorGUILayout.LabelField("Inspector", EditorStyles.boldLabel);
        EditorGUI.LabelField(new Rect((pos.width - dimensions.x) / 2f, pos.y, dimensions.x, dimensions.y), title, EditorStyles.boldLabel);
    }

    public override void OnInspectorGUI()
    {
        serializedObject.Update();
        dialogList.DoLayoutList();
        serializedObject.ApplyModifiedProperties();
    }

    private void AddDropDown(Rect buttonRect, ReorderableList list)
    {
        GenericMenu menu = new GenericMenu();

        menu.AddItem(new GUIContent("Dialog"), false, clickHandler, 0);
        menu.AddItem(new GUIContent("Wait"), false, clickHandler, 1);
        menu.AddItem(new GUIContent("Trigger"), false, clickHandler, 2);
        menu.AddItem(new GUIContent("Delegate"), false, clickHandler, 3);
        menu.ShowAsContext();
    }

    private void clickHandler(object target) {
        int selection = (int)target;
        switch (selection)
        {
            case 0:
                dialogList.AddItem<DialogBlock>(ScriptableObject.CreateInstance<DialogBlock>());
                break;
            case 1:
                dialogList.AddItem<DialogWait>(ScriptableObject.CreateInstance<DialogWait>());
                break;
            case 2:
                dialogList.AddItem<DialogTrigger>(ScriptableObject.CreateInstance<DialogTrigger>());
                break;
            case 3:
                dialogList.AddItem<DialogDelegate>(ScriptableObject.CreateInstance<DialogDelegate>());
                break;
        }
        serializedObject.ApplyModifiedProperties();
    }
}
