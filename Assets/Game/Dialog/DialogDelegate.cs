﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class DialogDelegate : Dialog {

    [SerializeField]
    UnityEvent OnExecute = new UnityEvent();

    public override void Begin(MonoBehaviour go)
    {
        started = true;
        OnExecute.Invoke();
        complete = true;
    }
}
