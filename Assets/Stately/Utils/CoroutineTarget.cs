﻿using UnityEngine;
using System.Collections;

namespace ABXY.Stately.Utils
{
    /// <summary>
    /// Utility class. I do nothing yet I must exist... How existential!
    /// 
    /// Used as the Monobehaviour context to run coroutines when no context is supplied by the user
    /// </summary>
    public class CoroutineTarget : MonoBehaviour
    {
    }
}