﻿using UnityEngine;
using UnityEngine.Events;
using System.Collections.Generic;
namespace ABXY.Stately {


    /// <summary>
    /// Container class for a state. Designed to be used in conjunction with the StateMachine Class
    /// </summary>
    [System.Serializable]
    public class State {

        /// <summary>The name of this state</summary>
        [SerializeField]
        public string stateName = "New State";


        /// <summary>
        /// Events in this callback are called when this state is activated
        /// </summary>
        public UnityEvent OnActivate = new UnityEvent();


        /// <summary>
        /// Events in this coroutine list are called when this state is activated
        /// </summary>
        public List<System.Collections.IEnumerable> onActivateCoroutines = new List<System.Collections.IEnumerable>();


        /// <summary>
        /// Events in this callback are called when this state is deactivated
        /// </summary>
        public UnityEvent OnDeactivate = new UnityEvent();


        /// <summary>
        /// Events in this coroutine list are called when this state is deactivated
        /// </summary>
        public List<System.Collections.IEnumerable> onDeactivateCoroutines = new List<System.Collections.IEnumerable>();


        /// <summary>
        /// Events in this callback are called while this state is activated
        /// </summary>
        public UnityEvent WhileActivated = new UnityEvent();
 

        /// <summary>
        /// Events in this coroutine list are called while this state is activated
        /// </summary>
        public List<System.Collections.IEnumerable> whileActivatedCoroutines = new List<System.Collections.IEnumerable>();

        /// <summary>
        /// Variable for user interface use. Stores whether this state is expanded in the UI. Hiding errors
        /// because this is accessed through Unity's serialization system, not by variable
        /// </summary>
#pragma warning disable 0414
        [SerializeField, HideInInspector]
        private bool expanded = true;
#pragma warning restore 0414


        /// <summary>
        /// The index of this state in the state machines enum definition
        /// </summary>
        public int enumIndex = -1;


        /// <summary>is activation of this state disabled?</summary>
        [SerializeField]
        public bool enabled = true;


        /// <summary>
        /// Constructor
        /// </summary>
        public State() {
            enabled = true;
        }

        /// <summary>
        /// Constructor - allows the user to set the state name
        /// </summary>
        /// <param name="enumIndex">The index of this state in the state machines enum definition</param>
        /// <param name="name">The name of the state. Must match the name of the state in the state 
        /// machine's enum definition</param>
        public State(int enumIndex, string name)
        {
            enabled = true;
            stateName = name;
            this.enumIndex = enumIndex;
        }
    }

}