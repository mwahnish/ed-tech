﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ABXY.Stately
{
    /// <summary>
    /// Enumerators for the Stately state machine system
    /// </summary>
    public static class StatelyEnums
    {

        /// <summary>
        /// State call types
        /// </summary>
        public enum HandlerType
        {
            OnActivate,
            WhileActivated,
            OnDeactivate
        }
    }
}
