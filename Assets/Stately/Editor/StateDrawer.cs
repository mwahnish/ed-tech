﻿using UnityEngine;
using UnityEditor;
using ABXY.Stately;

namespace ABXY.Stately.Editor
{

    /// <summary>
    /// Custom drawer for displaying individual states
    /// </summary>
    [CustomPropertyDrawer(typeof(State))]
    public class StateDrawer : PropertyDrawer
    {
        /// <summary>
        /// Drawing the drawer
        /// </summary>
        /// <param name="position"></param>
        /// <param name="property"></param>
        /// <param name="label"></param>
        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            EditorGUI.BeginProperty(position, label, property);
            
            // Grabbing the name of the state from the target object, as well as whether or not it is expanded in the UI
            SerializedProperty StateName = property.FindPropertyRelative("stateName");
            SerializedProperty expandedProperty = property.FindPropertyRelative("expanded");

            // Drawing the foldout tab and the label
            expandedProperty.boolValue = EditorGUI.Foldout(new Rect(position.x,position.y,position.width, 20f), expandedProperty.boolValue, "");
            position = EditorGUI.PrefixLabel(position, GUIUtility.GetControlID(FocusType.Passive), new GUIContent(StateName.stringValue));

            // Drawing the unity event callbacks
            if (expandedProperty.boolValue) {

                position.y += 25;

                // Drawing OnActivate
                SerializedProperty OnActivate = property.FindPropertyRelative("OnActivate");
                EditorGUI.PropertyField(new Rect(45f, position.y, position.x - position.width, position.height), OnActivate, new GUIContent(OnActivate.name), true);

                position.y += EditorGUI.GetPropertyHeight(OnActivate);


                // Drawing OnDeactivate
                SerializedProperty OnDeactivate = property.FindPropertyRelative("OnDeactivate");
                EditorGUI.PropertyField(new Rect(45f, position.y, position.x - position.width, position.height), OnDeactivate, new GUIContent(OnDeactivate.name), true);

                position.y += EditorGUI.GetPropertyHeight(OnDeactivate);


                // Drawing WhileActivated
                SerializedProperty WhileActivated = property.FindPropertyRelative("WhileActivated");
                EditorGUI.PropertyField(new Rect(45f, position.y, position.x - position.width, position.height), WhileActivated, new GUIContent(WhileActivated.name), true);
            }
            EditorGUI.EndProperty();
        }

        /// <summary>
        /// Calculating height for this drawer
        /// </summary>
        /// <param name="property"></param>
        /// <param name="label"></param>
        /// <returns></returns>
        public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
        {
            float height = 0f;

            // Height of the dropdown and label
            SerializedProperty StateName = property.FindPropertyRelative("stateName");
            height += EditorGUI.GetPropertyHeight(StateName);

            SerializedProperty expandedProperty = property.FindPropertyRelative("expanded");
            if (expandedProperty.boolValue)
            {

                // Height of OnActivate
                SerializedProperty OnActivate = property.FindPropertyRelative("OnActivate");
                height += EditorGUI.GetPropertyHeight(OnActivate);

                // Height of OnDeactivate
                SerializedProperty OnDeactivate = property.FindPropertyRelative("OnDeactivate");
                height += EditorGUI.GetPropertyHeight(OnDeactivate);

                // Height of WhileActivated
                SerializedProperty WhileActivated = property.FindPropertyRelative("WhileActivated");
                height += EditorGUI.GetPropertyHeight(WhileActivated);

                height += 25;

            }
            return height;

        }
    }
}
